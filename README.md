# Pong
Play the classic game of pong against the computer on one screen, or play multiplayer by adding more devices as paddles. 

### Installation
Node.js must be installed as a pre-requisite to run the project. You can download it from [here](https://nodejs.org/en/download/).


### Usage
1. Run this command to run the project.
```
node index.js
```

2. If everything is working correctly, you should see this:
```
Listen on port 8112!
```

3. If not, try `npm install` and run `node index.js` again.

4. Once it is working, type `https://localhost:8112` into the address bar of a browser on the computer with the terminal. You should see two lines saying `Player count: 1` and `SPACE TO START`.


### Playing the Game
##### Adding Players
1. Select 1, 2, 3, or 4 on the keyboard for the number of players playing (upto 4 players).
    * Player 1 - "w" for up and "s" for down
    * Player 2 - Up arrow and down arrow
    * Player 3 - "u" for up and "j" for down
    * Player 4 - Numpad 8 for up and numpad 5 for down

##### Adding handheld paddles
**NOTE: You can only add paddles on Android devices, not iOS.**
1. To connect an Android device to control the paddle on the left, first make sure both devices are on the same network. 

2. Type `https://IP_ADDRESS:8112/controller` into the address bar.

3. Holding the device upright, tilt it back to move the paddle upwards and tilt it forward to move the paddle downwards. 

##### Useful Keys
1. [Space] to start
2. "p" to pause
3. "f" to display frames per second

### Other Features
##### Extending the Gaming Area
1. Type `https://IP_ADDRESS:8112` into however many browser tabs wanted to extend the gaming area. Make sure that these devices are on the same network. 




